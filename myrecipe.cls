\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{myrecipe}

\LoadClass{report}

\RequirePackage{ifthen}
\RequirePackage{trimspaces}
\RequirePackage{etoolbox}
\RequirePackage{amsmath}
\RequirePackage{parskip}
\RequirePackage{cooking}
\RequirePackage{siunitx}
\RequirePackage[margin=2.5cm]{geometry}

% Setup spacing
\setlength{\parskip}{0.5em}
\setlength{\parindent}{0em}
\setlength{\recipemargin}{.3\textwidth}

% Large title font
\renewcommand{\recipetitle}{\section}

% Add lines around the recipe
\let\oldrecipe\recipe
\let\oldendrecipe\endrecipe
\renewenvironment{recipe}[1]{%
    \oldrecipe{#1}%
    \vspace{0.5em}%
    \hrule%
}{%
    \oldendrecipe%
    \vspace{1em}%
    \hrule%
}

% A title describing what sections are making
\newcommand{\forthe}[1]{\textbf{For the #1}}

% Step command - used for sets of ingredients,
% e.g.
% \step{
%     Rocks,
%     Stones,
%     Sticks
% }{
%     Mix the rocks and stones using the sticks.
% }
\newcommand{\doforingredient}[1]{$\lhook\joinrel\rightarrow$ \hfill #1 \\}

\newcommand{\step}[2]{%
    \ingredient{\parbox[t]{\textwidth}{%
        \ifthenelse{\equal{#1}{}}
            {}
            {%
                \raggedleft%
                \forcsvlist{\doforingredient}{#1}%
            }
    }}
    \noindent\parbox[t]{\linewidth}{%
        \setlength{\parskip}{0.5em}%
        \trim@spaces{#2}%
    }
}

% Translate the {cooking} package
\renewcommand{\energy}[1]{%
  \item Energy per Portion~\SI{#1}{\kilo\joule}%
}
\renewcommand{\preparationtime}[1]{%
  \item[Perparation Time] #1%
}
\newcommand{\cookingtime}[1]{%
  \item[Cooking Time] #1%
}
\renewcommand{\modification}[1]{%
  \item[Modification] \textit{\trim@spaces{#1}}%
}

% Useful commands ...

%% Tilde sign
\newcommand{\around}{%
    {\raisebox{.17ex}{\hbox{$\scriptstyle\mathtt{\sim}$}}}%
}

%% Temperature
\newcommand{\tempC}[1]{%
    \SI{#1}{\celsius}%
}

%% To allow comma escapes in ingredient lists
\newcommand{\comma}[0]{,\ }

%% New SI units ...
\DeclareSIUnit\tablespoon{tbsp}
\DeclareSIUnit\teaspoon{tsp}
