PDFDIR=pdf/

ifndef DISP
ifneq ($(MAKECMDGOALS),all)
DISP=YES
else
DISP=NO
endif
endif

ifndef LATEX
ifeq ($(DISP),YES)
LATEX=latexmk --pdf --pvc
else
LATEX=pdflatex
endif
endif

TEXFILES=$(wildcard *.tex)
PDFFILES=$(patsubst %.tex,$(PDFDIR)%.pdf,$(TEXFILES))

$(PDFDIR)%.pdf: %.tex myrecipe.cls
	@echo Generating $< ...
	@mkdir -p $(@D)
	$(LATEX) -output-directory=$(@D) $<
	@echo Done!

$(PDFDIR)book.pdf: $(TEXFILES)
book: $(PDFDIR)book.pdf

all: $(PDFFILES)

.DEFAULT_GOAL=book
.PRECIOUS: $(PDFDIR)book.pdf
